package com.github.churakovia.dhkafkahomework.consumer;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.PropertySource;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.annotation.TopicPartition;
import org.springframework.kafka.support.KafkaHeaders;
import org.springframework.messaging.handler.annotation.Header;
import org.springframework.messaging.handler.annotation.Payload;

@Slf4j
@SpringBootApplication
@PropertySource("classpath:env.properties")
public class DhKafkaHomeworkConsumerApplication {

    public static void main(String[] args) {
        SpringApplication.run(DhKafkaHomeworkConsumerApplication.class, args);
    }

    /*    https://docs.spring.io/spring-kafka/reference/html/#manual-assignment     */
    @KafkaListener(id = "g1", topicPartitions = @TopicPartition(topic = "${topic.test.name}", partitions = "0"))
    public void listener(@Payload String message,
                         @Header(KafkaHeaders.RECEIVED_TOPIC) String topic,
                         @Header(KafkaHeaders.RECEIVED_MESSAGE_KEY) String key,
                         @Header(KafkaHeaders.RECEIVED_PARTITION_ID) int partition) {
        log.info("Listener1 received message (topic: '{}', key: '{}', partition {}): '{}'", topic, key, partition, message);
    }

    @KafkaListener(id = "g2", topicPartitions = @TopicPartition(topic = "${topic.test.name}", partitions = "1"))
    public void listener2(@Payload String message,
                          @Header(KafkaHeaders.RECEIVED_TOPIC) String topic,
                          @Header(KafkaHeaders.RECEIVED_MESSAGE_KEY) String key,
                          @Header(KafkaHeaders.RECEIVED_PARTITION_ID) int partition) {
        log.info("Listener2 received message (topic: '{}', key: '{}', partition {}): '{}'", topic, key, partition, message);
    }

    @KafkaListener(id = "g3", topics = "${topic.single.partition.name}")
    public void listener3(@Payload String message,
                          @Header(KafkaHeaders.RECEIVED_TOPIC) String topic,
                          @Header(KafkaHeaders.RECEIVED_MESSAGE_KEY) String key,
                          @Header(KafkaHeaders.RECEIVED_PARTITION_ID) int partition) {
        log.info("Listener3 received message (topic: '{}', key: '{}', partition {}): '{}'", topic, key, partition, message);
    }
}
