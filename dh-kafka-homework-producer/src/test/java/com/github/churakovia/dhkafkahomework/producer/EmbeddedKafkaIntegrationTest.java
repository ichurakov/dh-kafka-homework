package com.github.churakovia.dhkafkahomework.producer;

import com.github.churakovia.dhkafkahomework.producer.service.KafkaProducerService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Import;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.test.context.EmbeddedKafka;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

import static org.apache.commons.lang3.RandomStringUtils.randomAlphabetic;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

@SpringBootTest
@Import(EmbeddedKafkaIntegrationTest.KafkaConsumer.class)
@DirtiesContext
@EmbeddedKafka(partitions = 1, brokerProperties = {"listeners=PLAINTEXT://localhost:9092", "port=9092"})
@ActiveProfiles("test")
class EmbeddedKafkaIntegrationTest {

    @Autowired
    private KafkaProducerService producer;

    @Autowired
    private KafkaConsumer consumer;

    @Value("${topic.test.name}")
    private String topicName;

    @BeforeEach
    void setup() {
        consumer.resetLatch();
    }

    @Test
    void sendTest() throws InterruptedException {
        final String givenPayload = randomAlphabetic(10);

        producer.send(topicName, givenPayload);

        boolean msgConsumed = consumer.getLatch().await(1000, TimeUnit.MILLISECONDS);
        assertTrue(msgConsumed);
        assertEquals(givenPayload, consumer.getPayload());
    }

    /**
     * https://www.baeldung.com/spring-boot-kafka-testing
     */
    static class KafkaConsumer {
        private CountDownLatch latch = new CountDownLatch(1);
        private String payload;

        @KafkaListener(topics = {"${topic.test.name}"}, groupId = "testConsumer")
        public void listener(String payload) {
            this.payload = payload;
            latch.countDown();
        }

        public void resetLatch() {
            latch = new CountDownLatch(1);
        }

        public CountDownLatch getLatch() {
            return latch;
        }

        public String getPayload() {
            return payload;
        }
    }
}