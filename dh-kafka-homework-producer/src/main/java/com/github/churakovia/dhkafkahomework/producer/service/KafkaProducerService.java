package com.github.churakovia.dhkafkahomework.producer.service;

import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import lombok.extern.slf4j.Slf4j;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Service
@Slf4j
@RequiredArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
public class KafkaProducerService {

    KafkaTemplate<String, String> kafkaTemplate;

    public void send(String topicName, String payload) {
        kafkaTemplate.send(topicName, UUID.randomUUID().toString(), payload);
        log.info("Send data to topic {}: '{}'", topicName, payload);
    }
}
