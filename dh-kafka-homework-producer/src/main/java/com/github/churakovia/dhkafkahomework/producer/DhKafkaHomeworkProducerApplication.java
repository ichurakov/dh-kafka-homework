package com.github.churakovia.dhkafkahomework.producer;

import com.github.churakovia.dhkafkahomework.producer.service.KafkaProducerService;
import com.github.churakovia.dhkafkahomework.producer.worker.ProducerWorker;
import org.apache.kafka.clients.admin.NewTopic;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Profile;
import org.springframework.context.annotation.PropertySource;
import org.springframework.kafka.config.TopicBuilder;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

@SpringBootApplication
@PropertySource("classpath:env.properties")
public class DhKafkaHomeworkProducerApplication {

    private static final ScheduledExecutorService POOL1 = Executors.newScheduledThreadPool(1);
    private static final ScheduledExecutorService POOL2 = Executors.newScheduledThreadPool(1);

    @Value("${topic.test.name}")
    private String topicTestName;

    public static void main(String[] args) {
        SpringApplication.run(DhKafkaHomeworkProducerApplication.class, args);
    }

    @Bean
    @Profile("!test")
    public CommandLineRunner runner(KafkaProducerService producer,
                                    @Value("${topic.test.send.timeout.ms}") long topicTestSendTimeout,
                                    @Value("${topic.single.partition.name}") String topicSinglePartitionName,
                                    @Value("${topic.single.partition.send.timeout.ms}") long topicSinglePartitionSendTimeout) {
        return args -> {
            POOL1.scheduleWithFixedDelay(ProducerWorker.from(topicTestName, producer), 0, topicTestSendTimeout, TimeUnit.MILLISECONDS);
            POOL2.scheduleWithFixedDelay(ProducerWorker.from(topicSinglePartitionName, producer), 0, topicSinglePartitionSendTimeout, TimeUnit.MILLISECONDS);
        };
    }

    @Bean
    public NewTopic topic(@Value("${topic.test.partitions}") int topicTestPartitions) {
        return TopicBuilder
                .name(topicTestName)
                .partitions(topicTestPartitions)
                .replicas(1)
                .build();
    }
}
