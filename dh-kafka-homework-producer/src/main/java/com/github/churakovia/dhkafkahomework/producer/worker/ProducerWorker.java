package com.github.churakovia.dhkafkahomework.producer.worker;

import com.github.churakovia.dhkafkahomework.producer.service.KafkaProducerService;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.experimental.FieldDefaults;
import lombok.extern.slf4j.Slf4j;

import static org.apache.commons.lang3.RandomStringUtils.randomAlphabetic;

@Slf4j
@RequiredArgsConstructor(staticName = "from")
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
public class ProducerWorker implements Runnable {

    String topicName;
    KafkaProducerService producer;

    @SneakyThrows
    @Override
    public void run() {
        final String data = "Hello from producer: '" + randomAlphabetic(10) + "'";
        producer.send(topicName, data);
    }
}
